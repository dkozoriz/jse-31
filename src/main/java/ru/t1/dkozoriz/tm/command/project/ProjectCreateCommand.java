package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public ProjectCreateCommand() {
        super("project-create", "create new project.");
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

}
