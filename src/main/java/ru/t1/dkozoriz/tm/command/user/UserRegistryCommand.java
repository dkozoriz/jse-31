package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.IAuthService;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public UserRegistryCommand() {
        super("user-registry", "registry user.");
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        showUser(user);
    }

}