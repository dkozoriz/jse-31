package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.IProjectTaskService;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public AbstractTaskCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    public String getArgument() {
        return null;
    }

    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("PROJECTID: " + task.getProjectId());
    }

    protected void renderTasks(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}