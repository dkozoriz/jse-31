package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    public TaskShowByProjectIdCommand() {
        super("task-show-by-project-id", "show task by project id.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

}
