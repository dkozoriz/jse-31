package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public ApplicationAboutCommand() {
        super("about", "show developer info.", "-a");
    }

    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + getServiceLocator().getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getServiceLocator().getPropertyService().getAuthorEmail() + "\n");

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + getServiceLocator().getPropertyService().getApplicationName() + "\n");

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + getServiceLocator().getPropertyService().getGitBranch());
        System.out.println("COMMIT ID: " + getServiceLocator().getPropertyService().getGitCommitId());
        System.out.println("COMMITTER NAME: " + getServiceLocator().getPropertyService().getGitCommitterName());
        System.out.println("COMMITTER EMAIL: " + getServiceLocator().getPropertyService().getGitCommitterEmail());
        System.out.println("MESSAGE: " + getServiceLocator().getPropertyService().getGitCommitMessage());
        System.out.println("TIME: " + getServiceLocator().getPropertyService().getGitCommitTime());
    }

}