package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public ApplicationExitCommand() {
        super("exit", "close application.");
    }

    public void execute() {
        System.exit(0);
    }

}