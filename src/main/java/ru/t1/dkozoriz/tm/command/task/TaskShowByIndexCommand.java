package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    public TaskShowByIndexCommand() {
        super("task-show-by-index", "show task by index.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Task task = getTaskService().findByIndex(userId, index);
        showTask(task);
    }

}
