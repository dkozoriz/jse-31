package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public ProjectRemoveByIdCommand() {
        super("project-remove-by-id", "remove project by id.");
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, id);
    }

}
