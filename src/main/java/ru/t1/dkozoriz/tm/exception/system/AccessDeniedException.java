package ru.t1.dkozoriz.tm.exception.system;

public final class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }

}