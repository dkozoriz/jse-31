package ru.t1.dkozoriz.tm.model.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends BusinessModel implements IWBS {

    @Nullable
    private String projectId;

    public Task(@NotNull final String name) {
        super(name);
    }

    public Task(@NotNull final String name, @NotNull final Status status) {
        super(name, status);
    }

}